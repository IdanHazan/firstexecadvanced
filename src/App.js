
import ShowItem from "./components/showItem";

function App() {

  const items = [
    { name: 'Sofa', price: 30, image: '/sofa.jpg' },
    { name: 'Table', price: 15, image: '/table.png' },
    { name: 'Chair', price: 55, image: '/chair.jpg' },
    { name: 'Tv', price: 93, image: '/tv.jpg' }
  ];
  return (
    <div>
      <div style={{display:'flex',justifyContent:'center'}}>
      <h1> My shopping list</h1>
      </div>
      {items.map((item) => 
        <ShowItem key={item.name}
          name={item.name}
          price={item.price}
          image={item.image}
        ></ShowItem>
      )}
    </div>
  );
}

export default App;
