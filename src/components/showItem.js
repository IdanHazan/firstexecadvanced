import './showItem.css'

function ShowItem(props) {
    return (
        <div className='show-item'>
            <div className="title__price">
                <div className="item__title"> {props.name}</div>
                <div className="item__price"> {props.price}</div>
            </div>
            <img src={props.image} width="150" height="100"></img>
        </div>
    );
}

export default ShowItem;